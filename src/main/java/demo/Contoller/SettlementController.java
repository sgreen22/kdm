package demo.Contoller;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Map;


@Controller  //rest controller returns string, not the content
@EnableAutoConfiguration
public class SettlementController {

//    @RequestMapping("/") //the root
//    String home() {
//        return "Howdy";
//    }

    @RequestMapping("/settlement")
    public String settlement(Map<String, Object> model) {
        model.put("message", "Welcome to your settlements");
        return "settlement";
    }
}
