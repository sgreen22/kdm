package demo.Contoller;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Map;

@Controller
@EnableAutoConfiguration
public class WelcomeController {

    @RequestMapping("/")
    public String welcome(Map<String, Object> model) throws Exception {
        model.put("message", "Welcome to KD:Manager");
        initPersonTable();
        selectFromPersonTable();
        return "welcome";
    }

//    db: hartwell_fatcatjava
//    user: hartwell_fatcatjava
//    pw: LD8xZn8h0Rd7

        public void initPersonTable() throws Exception {
            String url = "jdbc:mysql://213.175.217.93:3306/hartwell_fatcatjava";
            String username = "hartwell_fatcatjava";
            String password = "LD8xZn8h0Rd7";
            String driver = "com.mysql.jdbc.Driver";

            Class.forName(driver);
            Connection conn = DriverManager.getConnection(url, username, password);
            try {
                Statement statement = conn.createStatement();

                String sql = "CREATE TABLE `Person` (" +
                        "  `id` int(11) unsigned NOT NULL AUTO_INCREMENT," +
                        "  `name` varchar(20) NOT NULL DEFAULT ''," +
                        "  `password` varchar(20) DEFAULT NULL," +
                        "  PRIMARY KEY (`id`))";

                statement.executeUpdate(sql);
            } finally {
                conn.close();
            }
        }

    public void selectFromPersonTable() throws Exception {
        String url = "jdbc:mysql://213.175.217.93:3306/hartwell_fatcatjava";
        String username = "hartwell_fatcatjava";
        String password = "LD8xZn8h0Rd7";
        String driver = "com.mysql.jdbc.Driver";

        Class.forName(driver);
        Connection conn = DriverManager.getConnection(url, username, password);
        try {
            Statement statement = conn.createStatement();

            String sql = "SELECT * FROM `Person`";

            ResultSet rs = statement.executeQuery(sql);
            while(rs.next()) {
                System.out.println(rs.getObject(1));
            }
        } finally {
            conn.close();
        }
    }

}
